# go.path

go.path is a small utility that allows Makefiles and scripts
to easily pull the correct folder for cloning, downloading
or building go projects.

## Providers

Three providers are included in the project, you can implement
your own if necessary.

Simply implement the `PathProvider` interface and set the global
variable in the `gopath` subpackage, then call the CLI.

The included providers are executed in this order:

* Environment
* Config
    * `~` Config
    * `/etc/` Config
    * `.` Config
* CLI

The `--disable` flag allows one to disable certain providers.

Note that if no provider can return a good path, the binary fails
with exit code 5.

### Environment Provider 

This provider accepts one parameter: `--env-proj`

If this flag is present, the provider will prefer a second
gopath in the environment variable `GOPATH` if it is present.

### Config Provider

The config provider will check if any of the following files
is present:

* `~/.go.path`
* `/etc/go.path`
* `./.go.path`

The first file from this list that exists is read and return on 
stdout, no filtering, formatting or parsing.

### CLI Provider

This provider simply returns the parameter `--gopath` if it was
supplied.

The provider may be used for fallbacks if no gopath is set and
no config provided either, for example if go hasn't been installed
and configured.

If `--allow-defpath` is set or given then it will default to `~/go`
as a gopath. Beware that this won't allow the tool to fail since
this provider always *has* a value. Note that this tool also may
fail due to other reasons.

If possible, use `--gopath=~/go/`.

## Examples

`go.path --env-proj --gopath=~/go/`

If the GOPATH environment variable is set to `~/libs:~/projects`
then this will return `~/projects`, if no GOPATH is set it will
return whatever is written in one of the config files and if *those*
aren't present it will deault to `~/go`

### Actual Real Life Examples

Consider the following script:

`git clone git@git.example.org:user/repo $GOPATH/src/git.example.org/user/repo`

This line is problematic. It is not exactly portable, it reacts badly to
dual GOPATHs and lastly if no gopath is present, the repo will be cloned
into the root fs.

go.path fixes that:

`git clone git@git.example.org:user/repo $(go.path --allow-defpath)/src/git.example.org/user/repo`

This will always return a sane gopath to use.