package gopath

import "os/user"

// PathProvider is a generic interface to check if a source for the gopath
// is present and return it's value
type PathProvider interface {
	Has() bool    // Has must return true if the config is being used
	Get() string  // Get must return the most dominant string of the PathProvider
	Name() string // Name must return a short name without spaces of the provider
}

// EnabledProviders is a list of providers that this package has
// enabled. You can append to this or modify in init() methods if
// other providers are necessary
var EnabledProviders = []PathProvider{
	Env,
	Cfg,
	CLI,
}

var homeDir string

func init() {
	usr, err := user.Current()
	if err != nil {
		panic(err)
	}
	homeDir = usr.HomeDir
}
