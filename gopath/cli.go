package gopath

import (
	"flag"
	"path/filepath"
)

var allowDefaultGoPath bool

func init() {
	flag.String("gopath", "", "CLI Provided Gopath")
	flag.BoolVar(&allowDefaultGoPath, "allow-defpath", false, "If present or true the tool will fallback to the default Gopath. Use with care")
}

var CLI = CLIProvider{}

type CLIProvider struct{}

func (CLIProvider) Has() bool {
	if flag.Parsed() {
		if allowDefaultGoPath {
			return true
		}
		fl := flag.Lookup("gopath")
		if fl == nil {
			return false
		}
		if fl.Value.String() != "" {
			return true
		}
		return false
	}
	panic("Flags not parsed")
}

func (CLIProvider) Get() string {
	if l := flag.Lookup("gopath"); l == nil || l.Value.String() == "" {
		if allowDefaultGoPath {
			return filepath.Join(homeDir, "go")
		}
		panic("Something went horribly wrong")
	}
	return flag.Lookup("gopath").Value.String()
}

func (CLIProvider) Name() string {
	return "cli"
}
