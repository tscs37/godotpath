package gopath

import "os"
import "strings"
import "flag"

var Env = EnvironmentProvider{}

var useProjectFolder bool

func init() {
	flag.BoolVar(&useProjectFolder,
		"env-proj",
		false, "Use 2nd Environment path if it exists")
}

type EnvironmentProvider struct{}

func (EnvironmentProvider) Has() bool {
	val, has := os.LookupEnv("GOPATH")
	if has {
		return val != "" || len(val) > 0
	}
	return false
}

func (EnvironmentProvider) Get() string {
	str := os.Getenv("GOPATH")
	if strings.Contains(str, ":") {
		lstr := strings.Split(str, ":")
		if useProjectFolder && len(lstr) == 2 {
			str = lstr[1]
		} else {
			str = lstr[0]
		}
	}
	return str
}

func (EnvironmentProvider) Name() string {
	return "env"
}
