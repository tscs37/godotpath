package gopath

import "os"
import "path/filepath"
import "io/ioutil"

var paths = []string{
	filepath.Join(homeDir, ".go.path"),
	// etc-go.path is linux/mac only
	"/etc/go.path",
	filepath.Join(".", ".go.path"),
}

// Cfg is the running instance of ConfigProvider
var Cfg = ConfigProvider{}

// ConfigProvider uses the files "~/.go.path", "/etc/go.path" and "./.go.path"
// in that order. The content of the files is the raw path, no JSON or TOML,
// just the raw string
type ConfigProvider struct{}

func init() {
	var err error
	for k, v := range paths {
		// If any weird or local paths are defined
		// make them absolute.
		paths[k], err = filepath.Abs(v)
		if err != nil {
			panic("Could not expand configuration path")
		}
	}
}

// Has checks if any configuration file is present, if yes it returns true
func (ConfigProvider) Has() bool {
	for _, v := range paths {
		_, err := os.Stat(v)
		// Return true if *any* config file is found
		if os.IsExist(err) {
			return true
		}
	}
	return false
}

// Get returns the contents of the first configuration file that exists
func (ConfigProvider) Get() string {
	// Iterate through all paths configured, on windows the /etc/ path will fail
	for _, v := range paths {
		_, err := os.Stat(v)
		// Continue if file exists
		if os.IsExist(err) {
			// Read file. If there is an error, try next file
			str, err := ioutil.ReadFile(v)
			if err != nil {
				continue
			}
			// Return raw file content
			return string(str)
		}
	}
	panic("There is no config file or it could not be read")
}

// Name returns "cfg", use this name to disable this provider
func (ConfigProvider) Name() string {
	return "cfg"
}
