package cli

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/tscs37/godotpath/gopath"
)

// CLI is the main method to utilize go.path
// The application binary will call this method
func CLI() {

	flag.String("disable", "", "Comma-seperated List of disabled providers")
	flag.Parse()

	disabledProvs := strings.Split(
		flag.Lookup("disable").Value.String(),
		",",
	)

	// Loop through all providers available and enabled
	for _, v := range gopath.EnabledProviders {
		// If provider is enabled and has the variable, execute it
		// and terminate
		if !isDisabled(v.Name(), disabledProvs) && v.Has() {
			fmt.Printf("%s", v.Get())
			return
		}
	}

	os.Exit(5)
}

// Checks if provided name is in the list of disabled names
func isDisabled(name string, disabled []string) bool {
	for _, k := range disabled {
		if name == k {
			return true
		}
	}
	return false
}
